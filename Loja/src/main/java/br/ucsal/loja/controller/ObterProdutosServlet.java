package br.ucsal.loja.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.ucsal.loja.dao.CarneDAO;
import br.ucsal.loja.model.Carne;

@WebServlet("/ObterProdutosServlet")
public class ObterProdutosServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		CarneDAO dao = new CarneDAO();
		Long id = Long.parseLong(request.getParameter("id"));
		Carne carne = dao.obter(id);
		request.setAttribute("carne", carne);
		RequestDispatcher dispatcher = request.getRequestDispatcher("visualizarProduto.jsp");
		dispatcher.forward(request, response);

	}

}
