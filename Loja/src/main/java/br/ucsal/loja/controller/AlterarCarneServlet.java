package br.ucsal.loja.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.ucsal.loja.dao.CarneDAO;
import br.ucsal.loja.model.Carne;

/**
 * Servlet implementation class AlterarProdutoServlet
 */

@WebServlet("/AlterarCarneServlet")
public class AlterarCarneServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AlterarCarneServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Carne carne = new Carne();
		carne.setNome(request.getParameter("nome"));
		carne.setValidade(request.getParameter("validade"));
		carne.setValorQuilo(Double.parseDouble(request.getParameter("valor_quilo")));
		CarneDAO dao=new CarneDAO();
		dao.altera(carne);
		
	}

}
