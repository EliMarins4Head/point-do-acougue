<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="shortcut icon"
	href="https://img.icons8.com/color/50/000000/steak.png">
<title>Listagem de cliente</title>
<link rel="stylesheet" href="/css/custom.css" />
<link rel="stylesheet" href="/css/loja.css" />
<link rel="stylesheet"
	href="/webjars/bootstrap/4.1.3/css/bootstrap.min.css" />
</head>
<body>
	<c:import url="topo.jsp"></c:import>

	<h3>lista de clientes</h3>
	<ul>
		<c:forEach var="cliente" items="${clientes}">
			<li>${cliente.nome_cliente}</li>
			<a href="AlterarClienteServlet?id=${cliente.id_cliente}"
				type="submit">Editar</a>
			<a href="DeletarClienteServlet?id=${cliente.id_cliente}"
				type="submit">Excluir</a>
		</c:forEach>
	</ul>
	<c:import url="rodape.jsp"></c:import>
</body>
</html>