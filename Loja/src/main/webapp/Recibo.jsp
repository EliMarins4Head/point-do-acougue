<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="shortcut icon"
	href="https://img.icons8.com/color/50/000000/steak.png">
<title>Point da Carne</title>
</head>
<body>
	<c:import url="topo.jsp"></c:import>

	<h1>Recibo</h1>
	<p>Nome Cliente:</p>
	Eli Marins
	<br>
	<p>Carnes pedidas:</p>
	- Picanha ================= R$350,00
	<br>- Maminha ================= R$200,00
	<br>Compra efetivada com sucesso.
	<br>
	<c:import url="rodape.jsp"></c:import>
</body>
</html>