<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="shortcut icon"
	href="https://img.icons8.com/color/50/000000/steak.png">
<title>Point da Carne</title>
<link rel="stylesheet" href="/css/custom.css" />
<link rel="stylesheet" href="/css/loja.css" />
<link rel="stylesheet"
	href="/webjars/bootstrap/4.1.3/css/bootstrap.min.css" />
</head>
<body>
	<c:import url="topo.jsp"></c:import>

	<form action="AlterarProduto" method="get">
		Digite o ID do produto: <br /> <input type="text" name="id">
		<p>
			<input type="submit" name="BTEnvia" value="Buscar">
		</p>
	</form>
	<c:import url="rodape.jsp"></c:import>
</body>
</html>