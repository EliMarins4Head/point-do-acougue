<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="shortcut icon"
	href="https://img.icons8.com/color/50/000000/steak.png">
<title>Point da Carne</title>
<link rel="stylesheet" href="/css/custom.css" />
<link rel="stylesheet" href="/css/loja.css" />
<link rel="stylesheet"
	href="/webjars/bootstrap/4.1.3/css/bootstrap.min.css" />
</head>
<body>
	<c:import url="topo.jsp"></c:import>

	<h1>FORMULARIO ALTERAR CARNE</h1>
	<form action="AlterarCarneServlet" method="post">
		<input type="hidden" name="id" value="${carne.id_carne}">
		Nome:<br> <input type="text" name="nome"
			value="${carne.nome_carne}"><br> Valor do kilo:<br>
		<input type="text" name="valor_quilo" value="${carne.valor_kilo}"><br>
		Validade:<br> <input type="text" name="validade"
			value="${carne.validade}"><br> <input type="submit"
			value="Submit"><br>
	</form>
	<c:import url="rodape.jsp"></c:import>
</body>
</html>