<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="ISO-8859-1"%>
	<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="shortcut icon"
	href="https://img.icons8.com/color/50/000000/steak.png">
<title>Point da Carne</title>
<link rel="stylesheet" href="/css/custom.css" />
<link rel="stylesheet" href="/css/loja.css" />
<link rel="stylesheet" href="/webjars/bootstrap/4.1.3/css/bootstrap.min.css" />
</head>
<body>
<c:import url="topo.jsp"></c:import>

	<h1>FORMULARIO ALTERAR CLIENTE</h1>
	<form action="AlterarClienteServlet" method="post">
		<input type="hidden" name="id" value="${cliente.id_cliente}">
		Nome:<br> <input type="text" name="nome" value="${cliente.nome_cliente}"><br>
		CPF:<br> <input type="text" name="cpf" value="${cliente.cpf}"><br>
		Endereco:<br> <input type="text" name="endereco" value="${cliente.end_cliente}"><br>
		Senha:<br> <input type="password" name="senha" value="${cliente.senha_cliente}"><br>
		<input type="submit" value="Submit"><br>
	</form>
<c:import url="rodape.jsp"></c:import>
</body>
</html>