<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<link rel="shortcut icon"
	href="https://img.icons8.com/color/50/000000/steak.png">

<link rel="stylesheet" href="/css/custom.css" />
<link rel="stylesheet" href="/css/loja.css" />
<link rel="stylesheet"
	href="/webjars/bootstrap/4.1.3/css/bootstrap.min.css" />

<title>Point da Carne</title>
</head>
<body>
	<header>
		<div class="collapse bg-dark" id="navbarHeader">
			<div class="container">
				<div class="row">
					<div class="col-sm-8 col-md-7 py-4">
						<h4 class="text-white">Informações sobre nós</h4>
						<p class="text-muted">Fundada no dia 08 de Agosto de 2017,
							hoje nós já temos 3 filiais. Nós vendemos todos os tipos de carne
							e temos os cortes mais variados do mercado. Nós oferecemos ao
							cliente Kits churrasco, açougueiros padronizados, entrega
							delivery e ainda aceitamos todos os cartões, faça já sua compra e
							aproveite nossos cortes com excelência e melhores preços do
							mercado, visando repassar maior conforto e qualidade aos nossos
							clientes.</p>
					</div>
					<div class="col-sm-4 offset-md-1 py-4">
						<h4 class="text-white">Contato</h4>
						<ul class="list-unstyled">
							<li><a href="#" class="text-white">Siga no Twitter</a></li>
							<li><a href="#" class="text-white">Curta no Facebook</a></li>
							<li><a href="#" class="text-white">Email</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<div class="navbar navbar-dark bg-danger text-red box-shadow divHome">
			<div class="container d-flex justify-content-between">
				<a href="#" class="navbar-brand d-flex align-items-center"> <strong>
						<a href="index.jsp"><img src="https://i.imgur.com/b6cAohC.jpg"
							height="64" width="200"></a>
				</strong>
				</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse"
					data-target="#navbarHeader" aria-controls="navbarHeader"
					aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
			</div>
		</div>
	</header>

	<script src="/webjars/jquery/3.3.1-1/jquery.min.js"></script>
	<script src="/webjars/popper.js/1.14.4/umd/popper.min.js"></script>
	<script src="/webjars/bootstrap/4.1.3/js/bootstrap.min.js"></script>
	<script src="/webjars/holderjs/2.5.2/holder.min.js"></script>
</body>
</html>